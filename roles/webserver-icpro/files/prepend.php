<?php
/** 
 * This script allows prepending a script for every php script that is requested through Apache
 *
 * It looks for a prepend.php file in the parent directory of the document root. If it 
 * exists, the prepend.php file will be executed.
 *
 * Note! This script will not be executed for cron jobs since they are not using Apache
 *
 * @author Adrian Meyer 2016-04-05
 */

if ( isset( $_SERVER['HTTP_HOST'] )) {
	// This is a non-CLI/cron request
	// Note: For CLI/cron php, the HTTP_HOST will not be set

	// Look for a prepend.php in parent folder of document root
	$prependFile = realpath( $_SERVER['DOCUMENT_ROOT'].'/..' ) . '/prepend.php';
	if ( file_exists( $prependFile )) {
		require_once( $prependFile );

	}
}
