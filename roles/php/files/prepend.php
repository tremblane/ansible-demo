<?php
  // -------------------------------------------------------------
  // written by Adrian Meyer 2016-04-05
  // -------------------------------------------------------------

  // for CLI/cron php the HTTP_HOST will not be set
  if (isset($_SERVER['HTTP_HOST'])) {

    // assume a prepend.php location in parent folder of document root
    $prependFile = realpath( $_SERVER['DOCUMENT_ROOT'].'/..' ).'/prepend.php';
    // check for prepend.php file in environments www folder
    if ( file_exists( $prependFile )) {
      require_once( $prependFile );
    }
  }

?>
