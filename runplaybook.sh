#!/bin/bash
######################################################################
# runplaybook.sh - runs an ansible playbook with standard options
#
# usage: runplaybook.sh <inventory filename> [<ansible-playbook options> ... ]
#
# example: runplaybook.sh production --tags sshd --limit host1
#
# author: Jason Dew
######################################################################


##### Configuration

adminBaseDir=/datastore/admin
ansibleBaseDir=$adminBaseDir/ansible
playbookDir=$ansibleBaseDir/playbooks
inventoryDir=$ansibleBaseDir/inventory

ansiblePlaybookCmd=/usr/bin/ansible-playbook

playbookPath=$playbookDir/site.yml

standardOptions="$playbookPath --ask-pass --user=root"

##### Main

##check for options
if [[ -z "$1" ]]; then
	echo "Error: no inventory file given"
	echo "Usage: `basename $0` <inventory file>"
	echo -n "Valid options: "
	echo $(ls $inventoryDir)
	exit 1
fi

##get inventory from input
inventoryFileName="$1"
shift #rest of parameters to be passed as-is

inventoryPath=$inventoryDir/$inventoryFileName

if [[ ! -e "$inventoryPath" ]]; then
	echo "Error: no inventory file found"
	echo -n "Valid options: "
	echo $(ls $inventoryDir)
	exit 1
fi

echo "Running: $ansiblePlaybookCmd $standardOptions -i $inventoryPath $*" #DEBUG

$ansiblePlaybookCmd $standardOptions -i $inventoryPath $*
exitCode=$?
exit $exitCode

##To run manually:
# /usr/bin/ansible-playbook playbooks/site.yml --ask-pass --user=root -i inventory/XXXX --limit=HOST
