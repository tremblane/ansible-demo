# Files

## `template-esx.ks`
Template used for kickstarting a server in an ESX environment. Change the hostname and IP addresses in the "CHANGEME" section.

## `runplaybook.sh`, `runplaybook-keychain.sh`
Shell script wrapper around the ansible-playbook command. The `runplaybook-keychain.sh` script expects a keychain file for root, to use an existing ssh-agent already running.

## `playbooks/site.yml`
The only playbook in this repository. The only thing done in this playbook is to assign roles to hosts based on their groups.

# Organization

This playbook utilizes roles to determine which tasks to apply to hosts. Every role has a hostgroup of the same name, so assigning a host to a hostgroup also applies the role of the same name.

Each environment (development, production, infrastructure, etc.) also has its own hostgroup with the same name. The "icp" environment corresponds to a group of three web development groups whose names started with an I, C, and P.

Websites can be configured by putting a host in the webserver-icp or webserver-common hostgroup. That host must then have a host_vars file created for it, which has the definitions for the websites.