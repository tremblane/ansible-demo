lang en_US
keyboard us
timezone America/New_York --isUtc
# Root password
rootpw --iscrypted $6$REDACTED
reboot
text
cdrom
#bootloader --location=mbr --append="rhgb quiet crashkernel=auto"
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda --append="net.ifnames=0"
#vvv CHANGEME vvv#
network --device=eth0 --bootproto=static --ip=1.2.3.X --netmask=255.255.255.0 --gateway=1.2.3.1 --nameserver=1.1.1.1
network --device=eth1 --bootproto=static --ip=192.169.1.X --netmask=255.255.254.0 --gateway=192.168.1.1 --nodefroute
network --hostname=CHANGEME.university.edu
#^^^ CHANGEME ^^^#
auth --passalgo=sha512 --enableldapauth --ldapserver=ldap://ldap.university.edu/ --ldapbasedn=dc=lbg,dc=unc,dc=edu --enablekrb5 --krb5realm=AD.UNIVERSITY.EDU --krb5kdc=krb0.university.edu,krb1.university.edu,krb2.university.edu --krb5adminserver=krba.university.edu
selinux --permissive
firewall --enabled --ssh
skipx
firstboot --disable
# Disk partitioning
#zerombr
#autopart
clearpart --all --initlabel
part /boot --fstype="xfs" --ondisk=sda --size=500
part pv.root --fstype="lvmpv" --ondisk=sda --size=2048 --grow
part pv.swap --fstype="lvmpv" --ondisk=sdb --size=2048 --grow
volgroup vg_root pv.root
volgroup vg_swap pv.swap
logvol / --fstype="xfs" --grow --size=2048 --name=lv_root --vgname=vg_root
logvol swap --fstype="swap" --grow --size=2048 --name=lv_swap --vgname=vg_swap

%packages
@base
dos2unix
nfs-utils
nss-pam-ldapd
ntp
pam_ldap
pam_krb5
redhat-lsb-core
vim-enhanced
wget
subscription-manager
%end

%post --log=/root/ks-post.log
mkdir -m0700 /root/.ssh/
wget -O /root/.ssh/authorized_keys https://kickstart.university.edu/files/authorized_keys
chmod 0600 /root/.ssh/authorized_keys
%end
