#!/bin/bash
######################################################################
# runplaybook.sh - runs an ansible playbook with standard options
#
# usage: runplaybook.sh <inventory filename> [<ansible-playbook options> ... ]
#
# example: runplaybook.sh production --tags sshd --limit host1
#
# author: Jason Dew
######################################################################


##### Configuration

adminBaseDir=/datastore/admin
ansibleBaseDir=$adminBaseDir/ansible
playbookDir=$ansibleBaseDir/playbooks
inventoryDir=$ansibleBaseDir/inventory

ansiblePlaybookCmd=/usr/bin/ansible-playbook

playbookPath=$playbookDir/site.yml

standardOptions="$playbookPath --user=root"

rootKeychain="/root/.keychain/$(hostname)-sh"

##### Main

##check for options
if [[ -z "$1" ]]; then
	echo "Error: no inventory file given"
	echo "Usage: `basename $0` <inventory file>"
	echo -n "Valid options: "
	echo $(ls $inventoryDir)
	exit 1
fi

##get inventory from input
inventoryFileName="$1"
shift #rest of parameters to be passed as-is

inventoryPath=$inventoryDir/$inventoryFileName

if [[ ! -e "$inventoryPath" ]]; then
	echo "Error: no inventory file found"
	echo -n "Valid options: "
	echo $(ls $inventoryDir)
	exit 1
fi

##check we have a running ssh-agent as root with a loaded key we can use
echo -n "Checking for root ssh-agent..."
if ! sudo bash -c "source $rootKeychain && ssh-add -l >/dev/null"; then
	echo "Error: cannot connect to ssh-agent"
	exit 1
fi

echo " root ssh-agent found"
echo

echo "Running: source $rootKeychain && $ansiblePlaybookCmd $standardOptions -i $inventoryPath $*" #DEBUG

sudo bash -c "source $rootKeychain && $ansiblePlaybookCmd $standardOptions -i $inventoryPath $*"
exitCode=$?
exit $exitCode

##To run manually:
# sudo bash -c "source /root/.keychain/$(hostname)-sh && /usr/bin/ansible-playbook playbooks/site.yml --user=root -i inventory/XXXX --limit=HOST"
